# Star Wars Animated Worlds

## The project
Star Wars Animated Worlds is a Star Wars planets wiki website. Here, the users can access to information about some planets of the Star Wars universe.

**To see the project running, go to: [https://swaw.vercel.app/](https://swaw.vercel.app/)**

## The skeleton
The tooling and criteria used for this project was:
* Create React App
* Axios
* Responsive Web Design
* Mobile First
* Swapi (Star Wars API)

## How to run the app?
`yarn start` runs the app in development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. 

## How it works?
Well, when you go into this website, you will find a carousel of the SW worlds. You can see more information about each planet if you click on the View Planet Info button.<br>

## Next steps
* **Add new sections:** like the diferents species or films
* **Use other api service to get new data**