import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import PlanetList from '../../components/PlanetList';
import PlanetInfo from '../../components/PlanetInfo';
import styles from './Planets.module.scss';

const Planets = () => {
  return (
    <div className={styles.container}>
      <Router>
        <Switch>

          <Route path="/PlanetInfo/:page/:id" component={PlanetInfo} />
          <Route path="/:page" component={PlanetList} />
          <Route exact path="/" component={PlanetList} />
        </Switch>
      </Router>
    </div>
  )
}

export default Planets;