import { Link } from "react-router-dom";
import PlanetImage from '../PlanetImage';
import PlanetQuote from '../PlanetQuote';
import styles from './PlanetCard.module.scss';

const PlanetCard = ({planet, id, page}) => {
  if (!planet) {
    return null;
  }

  return ( 
    <div className={styles.container}>
      <div className={styles.card}>
        <PlanetImage planet={planet.name} bigBottom={false}/>
        <div className={styles.textSection}>
          <h2 className={styles.planetName}>{planet.name}</h2>
          <PlanetQuote planet={planet.name} />
          <Link to={`/PlanetInfo/${page}/${id}`} className={styles.button}>View Planet Info</Link>
        </div>
      </div>
    </div>
  )
}

export default PlanetCard;