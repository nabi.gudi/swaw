import Alderaan from '../../assets/planets/Alderaan.png'
import Bespin from '../../assets/planets/Bespin.png'
import Coruscant from '../../assets/planets/Coruscant.png'
import Dagobah from '../../assets/planets/Dagobah.png'
import Endor from '../../assets/planets/Endor.png'
import Hoth from '../../assets/planets/Hoth.png'
import Kamino from '../../assets/planets/Kamino.png'
import Naboo from '../../assets/planets/Naboo.png'
import Tatooine from '../../assets/planets/Tatooine.png'
import YavinIV from '../../assets/planets/Yavin_IV.png'
import styles from './PlanetImage.module.scss';

export const PlanetImage = (props) => {
  switch (props?.planet){
    case 'Kashyyyk':
    case 'Stewjon':
    case 'Troiken':
    case 'Tholoth':
    case 'Quermia':
    case 'Alderaan':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Alderaan} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Socorro':
    case 'Geonosis':
    case 'Mustafar':
    case 'Bespin':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Bespin} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Sullust':
    case 'Malastare':
    case 'Dathomir':
    case 'Umbara':
    case 'Coruscant':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Coruscant} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Polis Massa':
    case 'Nal Hutta':
    case 'Ord Mantell': 
    case 'Dorin':
    case 'Zolan':
    case 'Dagobah':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Dagobah} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Shili':
    case 'Muunilinst':
    case 'Concord Dawn':
    case 'Saleucami':
    case 'Mygeeto':
    case 'Ryloth':
    case 'Endor':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Endor} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Bestine IV':
    case 'Tund':
    case 'Haruun Kal':
    case 'Iktotch':
    case 'Iridonia':
    case 'Hoth':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Hoth} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Cato Neimoidia':
    case 'Eriadu':
    case 'Corellia':
    case 'Unknown':
    case 'Mon Cala':
    case 'Chandrila':
    case 'Kamino':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Kamino} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      ); 
    case 'Champala':
    case 'Mirial':
    case 'Serenno':
    case 'Naboo':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Naboo} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      ); 
    case 'Utapau':
    case 'Felucia':
    case 'Rodia':
    case 'Trandosha':
    case 'Toydaria':
    case 'Cerea':
    case 'Glee Anselm':
    case 'Yavin IV':
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={YavinIV} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
    case 'Kalee':
    case 'Aleen Minor':
    case 'Vulpter':
    case 'Tatooine':
    case 'Dantooine':
    case 'Ojom':
    case 'Skako':
    default:
      return (
        <div className={props?.bigBottom ? styles.bigBottomContainer : styles.container }>
          <img src={Tatooine} alt={props.planet} className={props?.bigBottom ? styles.bigBottom : styles.image}/> 
        </div>
      );
  };
};

export default PlanetImage;