import React, { useState, useEffect } from 'react';
import { getPlanetsWithPage } from '../../api/swapi';
import { Carousel } from 'react-responsive-carousel';
import PlanetCard from '../PlanetCard';
import cleanURL from '../../utils/cleanUrl';
import Back from '../../assets/icons/back.svg';
import Next from '../../assets/icons/next.svg';
import styles from './PlanetList.module.scss';


const PlanetList = (props) => {
  const [currentPage, setCurrentPage] = useState( props.match.params.hasOwnProperty('page') ? parseInt(props.match.params.page) : 1);
  let [planets, setPlanets] = useState([]);

  const planetClick = () => {
    return planets.next === null ? setCurrentPage(1) : setCurrentPage(currentPage + 1)
  }

  useEffect(() => {
    getPlanetsWithPage(currentPage).then(response => {
      setPlanets(response);
    });
  }, [currentPage]);
  
  if (!planets?.results) {
    return null;
  }

  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Star Wars Planets</h1>
      <Carousel showThumbs={false} showStatus={false} useKeyboardArrows dynamicHeight={true} 
        renderArrowNext={(onClickHandler, hasNext, label) =>
          hasNext && (
            <button type="button" onClick={onClickHandler} title={label} className={styles.btnNext}>
                <img src={Next} alt='Next Planet' className={styles.arrow} />
            </button>
          )
        }
        renderArrowPrev={(onClickHandler, hasPrev, label) =>
          hasPrev && (
            <button type="button" onClick={onClickHandler} title={label} className={styles.btnLeft}>
              <img src={Back} alt='Previous Planet' className={styles.arrow}/>
            </button>
          )
        }
        infiniteLoop
      >
          {planets.results.map((planet) => <PlanetCard planet={planet} key={planet.name} id={cleanURL(planet.url)} page={currentPage}/>)}
      </Carousel>

      <button className={styles.button} onClick={() => planetClick()}>View more planets</button>

    </div>
  );
}

export default PlanetList;