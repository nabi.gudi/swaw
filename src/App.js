import './App.css';
import Planets from './pages/Planets';

function App() {
  return (
    <div className="App">
      <Planets />
    </div>
  );
}

export default App;
